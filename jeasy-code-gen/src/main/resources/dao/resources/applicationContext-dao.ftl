<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:master-slave="http://shardingsphere.io/schema/shardingsphere/masterslave"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://www.springframework.org/schema/aop
       http://www.springframework.org/schema/aop/spring-aop.xsd
       http://www.springframework.org/schema/util
       http://www.springframework.org/schema/util/spring-util.xsd
       http://shardingsphere.io/schema/shardingsphere/masterslave
       http://shardingsphere.io/schema/shardingsphere/masterslave/master-slave.xsd">

    <bean id="master0_datasource" class="${conf.basePackage}.base.datasource.FixedDruidDataSource" init-method="init"
          destroy-method="close">
        <!-- 基本属性 url、user、password -->
        <property name="url" value="${r'${'}master0.jdbc.url}"/>
        <property name="username" value="${r'${'}master0.jdbc.username}"/>
        <property name="password" value="${r'${'}master0.jdbc.password}"/>

        <!-- 配置初始化大小、最小、最大 -->
        <property name="initialSize" value="5"/>
        <!-- 连接池中最少空闲maxIdle个连接 -->
        <property name="minIdle" value="5"/>
        <!-- 连接池激活的最大数据库连接总数。设为0表示无限制 -->
        <property name="maxActive" value="20"/>
        <!-- 最大建立连接等待时间，单位为 ms，如果超过此时间将接到异常。设为-1表示无限制 -->
        <property name="maxWait" value="60000"/>
        <!-- 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 -->
        <property name="timeBetweenEvictionRunsMillis" value="60000"/>
        <!-- 配置连接池中连接可空闲的时间(针对连接池中的连接对象.空闲超过这个时间则断开，直到连接池中的连接数到minIdle为止)，单位是毫秒 -->
        <property name="minEvictableIdleTimeMillis" value="300000"/>
        <!-- 用来检测连接是否有效的sql，要求是一个查询语句 -->
        <property name="validationQuery" value="SELECT 'x' FROM DUAL"/>
        <!-- 建议配置为true，不影响性能，并且保证安全性 -->
        <property name="testWhileIdle" value="true"/>
        <!-- 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 -->
        <property name="testOnBorrow" value="false"/>
        <!-- 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 -->
        <property name="testOnReturn" value="false"/>
        <!-- 打开PSCache，并且指定每个连接上PSCache的大小(Oracle或mysql5.5及以上使用) -->
        <property name="poolPreparedStatements" value="true"/>
        <property name="maxPoolPreparedStatementPerConnectionSize" value="20"/>
        <!-- 配置监控统计拦截的filters -->
        <!--<property name="filters" value="stat,log4j,wall"/>-->
        <property name="proxyFilters">
            <list>
                <ref bean="stat-filter"/>
                <ref bean="slf4j-filter"/>
                <ref bean="wall-filter"/>
            </list>
        </property>
        <!-- 配置关闭长时间不使用的连接 -->
        <!-- 是否清理removeAbandonedTimeout秒没有使用的活动连接,清理后并没有放回连接池(针对未被close的活动连接) -->
        <property name="removeAbandoned" value="true"/>
        <!-- 活动连接的最大空闲时间,1800秒，也就是30分钟 -->
        <property name="removeAbandonedTimeout" value="1800"/>
        <!-- 连接池收回空闲的活动连接时是否打印消息 -->
        <property name="logAbandoned" value="true"/>
    </bean>

    <bean id="slave0_datasource" class="${conf.basePackage}.base.datasource.FixedDruidDataSource" init-method="init"
          destroy-method="close">
        <!-- 基本属性 url、user、password -->
        <property name="url" value="${r'${'}slave0.jdbc.url}"/>
        <property name="username" value="${r'${'}slave0.jdbc.username}"/>
        <property name="password" value="${r'${'}slave0.jdbc.password}"/>

        <!-- 配置初始化大小、最小、最大 -->
        <property name="initialSize" value="5"/>
        <!-- 连接池中最少空闲maxIdle个连接 -->
        <property name="minIdle" value="5"/>
        <!-- 连接池激活的最大数据库连接总数。设为0表示无限制 -->
        <property name="maxActive" value="20"/>
        <!-- 最大建立连接等待时间，单位为 ms，如果超过此时间将接到异常。设为-1表示无限制 -->
        <property name="maxWait" value="60000"/>
        <!-- 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 -->
        <property name="timeBetweenEvictionRunsMillis" value="60000"/>
        <!-- 配置连接池中连接可空闲的时间(针对连接池中的连接对象.空闲超过这个时间则断开，直到连接池中的连接数到minIdle为止)，单位是毫秒 -->
        <property name="minEvictableIdleTimeMillis" value="300000"/>
        <!-- 用来检测连接是否有效的sql，要求是一个查询语句 -->
        <property name="validationQuery" value="SELECT 'x' FROM DUAL"/>
        <!-- 建议配置为true，不影响性能，并且保证安全性 -->
        <property name="testWhileIdle" value="true"/>
        <!-- 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 -->
        <property name="testOnBorrow" value="false"/>
        <!-- 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能 -->
        <property name="testOnReturn" value="false"/>
        <!-- 打开PSCache，并且指定每个连接上PSCache的大小(Oracle或mysql5.5及以上使用) -->
        <property name="poolPreparedStatements" value="true"/>
        <property name="maxPoolPreparedStatementPerConnectionSize" value="20"/>
        <!-- 配置监控统计拦截的filters -->
        <!--<property name="filters" value="stat,log4j,wall"/>-->
        <property name="proxyFilters">
            <list>
                <ref bean="stat-filter"/>
                <ref bean="slf4j-filter"/>
                <ref bean="wall-filter"/>
            </list>
        </property>
        <!-- 配置关闭长时间不使用的连接 -->
        <!-- 是否清理removeAbandonedTimeout秒没有使用的活动连接,清理后并没有放回连接池(针对未被close的活动连接) -->
        <property name="removeAbandoned" value="true"/>
        <!-- 活动连接的最大空闲时间,1800秒，也就是30分钟 -->
        <property name="removeAbandonedTimeout" value="1800"/>
        <!-- 连接池收回空闲的活动连接时是否打印消息 -->
        <property name="logAbandoned" value="true"/>
    </bean>

    <bean id="wall-filter" class="com.alibaba.druid.wall.WallFilter">
        <property name="dbType" value="mysql"/>
    </bean>

    <bean id="stat-filter" class="com.alibaba.druid.filter.stat.StatFilter">
        <property name="slowSqlMillis" value="10000"/>
        <property name="logSlowSql" value="true"/>
        <property name="mergeSql" value="true"/>
    </bean>

    <bean id="slf4j-filter" class="com.alibaba.druid.filter.logging.Slf4jLogFilter">
        <property name="statementExecutableSqlLogEnable" value="true"/>
    </bean>

    <bean id="druid-stat-interceptor" class="com.alibaba.druid.support.spring.stat.DruidStatInterceptor"/>

    <aop:config proxy-target-class="true">
        <aop:advisor pointcut="execution(public * ${conf.basePackage}..dao.*.*(..))" advice-ref="druid-stat-interceptor"/>
        <aop:advisor pointcut="execution(public * ${conf.basePackage}..manager.*.*(..))" advice-ref="druid-stat-interceptor"/>
        <aop:advisor pointcut="execution(public * ${conf.basePackage}..service.*.*(..))" advice-ref="druid-stat-interceptor"/>
    </aop:config>

    <!--<bean id="dataSource" class="${conf.basePackage}.base.datasource.ThreadLocalRoutingDataSource">-->
    <!--<property name="defaultTargetDataSource" ref="master0_datasource"/>-->
    <!--<property name="targetDataSources">-->
    <!--<map>-->
    <!--<entry key="master0" value-ref="master0_datasource"/>-->
    <!--<entry key="slave0" value-ref="slave0_datasource"/>-->
    <!--</map>-->
    <!--</property>-->
    <!--</bean>-->

    <master-slave:data-source id="dataSource" master-data-source-name="master0_datasource"
                              slave-data-source-names="slave0_datasource"/>

    <bean id="sqlSessionFactory" class="com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean">
        <!-- 配置数据源 -->
        <property name="dataSource" ref="dataSource"/>
        <!-- 自动扫描 Xml 文件位置 -->
        <property name="mapperLocations" value="classpath:sqlMapper/*Mapper.xml"/>

        <!-- 插件配置 -->
        <property name="plugins">
            <array>
                <!-- 分页插件配置 -->
                <bean id="paginationInterceptor" class="com.baomidou.mybatisplus.plugins.PaginationInterceptor">
                    <!-- 指定数据库方言 -->
                    <property name="dialectType" value="mysql"/>
                </bean>
                <!-- SQL 执行分析拦截器 stopProceed 发现全表执行 delete update 是否停止运行 不建议生产环境配置 -->
                <!--<bean id="sqlExplainInterceptor" class="com.baomidou.mybatisplus.plugins.SqlExplainInterceptor"/>-->
                <!-- 乐观锁插件 -->
                <!--<bean id="optimisticLockerInterceptor" class="com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor"/>-->
                <!-- 性能拦截器，兼打印sql，不建议生产环境配置 -->
                <!--<bean id="performanceInterceptor" class="com.baomidou.mybatisplus.plugins.PerformanceInterceptor">-->
                <!--<property name="maxTime" value="100" />-->
                <!--&lt;!&ndash;SQL是否格式化 默认false&ndash;&gt;-->
                <!--<property name="format" value="true" />-->
                <!--</bean>-->
            </array>
        </property>

        <!-- MP 全局配置注入 -->
        <property name="globalConfig" ref="globalConfig"/>
        <!-- 配置 Mybatis 配置文件（可无） -->
        <property name="configuration" ref="mybatisConfig"/>
        <!--<property name="configLocation" value="classpath:sqlMapConfig.xml"/>-->

        <!-- 配置包别名: 自动取对应包中不包括包名的简单类名作为包括包名的别名 -->
        <!--<property name="typeAliasesPackage" value="${conf.basePackage}.*.entity"/>-->
    </bean>

    <bean id="mybatisConfig" class="com.baomidou.mybatisplus.MybatisConfiguration">
        <property name="useGeneratedKeys" value="true"/>
        <property name="defaultExecutorType" value="REUSE"/>
        <property name="lazyLoadingEnabled" value="true"/>
        <property name="defaultStatementTimeout" value="2500"/>
        <property name="mapUnderscoreToCamelCase" value="true"/>
        <!-- 部分数据库不识别默认的NULL类型（比如oracle，需要配置该属性 -->
        <property name="jdbcTypeForNull">
            <util:constant static-field="org.apache.ibatis.type.JdbcType.NULL"/>
        </property>
    </bean>

    <!-- 定义 MP 全局策略 -->
    <bean id="globalConfig" class="com.baomidou.mybatisplus.entity.GlobalConfiguration">
        <!-- 主键策略配置 -->
        <!-- 可选参数
            AUTO->`0`("数据库ID自增")
            INPUT->`1`(用户输入ID")
            ID_WORKER->`2`("全局唯一ID")
            UUID->`3`("全局唯一ID")
        -->
        <property name="idType" value="0"/>

        <!-- 数据库类型配置 -->
        <!-- 可选参数（默认mysql）
            MYSQL->`mysql`
            ORACLE->`oracle`
            DB2->`db2`
            H2->`h2`
            HSQL->`hsql`
            SQLITE->`sqlite`
            POSTGRE->`postgresql`
            SQLSERVER2005->`sqlserver2005`
            SQLSERVER->`sqlserver`
        -->
        <property name="dbType" value="mysql"/>

        <!-- 全局表为下划线命名设置 false -->
        <property name="dbColumnUnderline" value="false"/>

        <!-- SQL 自动注入逻辑处理器  -->
        <property name="sqlInjector" ref="logicSqlInjector"/>
        <property name="logicDeleteValue" value="1"/>
        <property name="logicNotDeleteValue" value="0"/>
    </bean>

    <bean id="logicSqlInjector" class="com.baomidou.mybatisplus.mapper.LogicSqlInjector"/>

    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="${conf.basePackage}.*.dao"/>
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
    </bean>

    <bean id="txManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!-- Monitor BeanPostProcessor -->
    <!--<bean id="daoTimeCostLoggerProcessor" class="${conf.basePackage}.base.dao.aspect.TimeCostDaoProcessor">-->
    <!--<property name="interceptor">-->
    <!--<bean class="${conf.basePackage}.base.dao.aspect.TimeCostDaoInterceptor" />-->
    <!--</property>-->
    <!--</bean>-->

    <!-- 切面配置：DAO层方法执行日志 -->
    <bean class="${conf.basePackage}.base.mybatis.aspect.DaoCostLogAspect"/>
</beans>
