import Vue from 'vue'
import iView from 'iview'
import VueRouter from 'vue-router'
import Util from '../libs/util'
import storage from '@/utils/helpers/storageLite'
import { iviewAdminRouter } from './iviewAdmin'
import { adminRouter } from './routes'

Vue.use(VueRouter)

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
  ...adminRouter,
  ...iviewAdminRouter
]

const router = new VueRouter({
  routes: routers,
  // 当hashbang的值为true时，所有的路径都会被格式化已#!开头
  hashbang: true,
  history: true,
  saveScrollPosition: true,
  suppressTransitionError: true
})

router.beforeEach((to, from, next) => {
  iView.LoadingBar.start()
  Util.title(to.meta.title)
  if (storage.get('locking') === '1' && to.name !== 'locking') {
    // 判断当前是否是锁定状态
    next({
      replace: true,
      name: 'locking'
    })
  } else if (storage.get('locking') === '0' && to.name === 'locking') {
    next(false)
  } else {
    if (!storage.get('user') && to.name !== 'login') {
      // 判断是否已经登录且前往的页面不是登录页
      next({
        name: 'login'
      })
    } else if (storage.get('user') && to.name === 'login') {
      // 判断是否已经登录且前往的是登录页
      Util.title()
      next({
        name: 'home_index'
      })
    } else {
      const curRouterObj = Util.getRouterObjByName([...routers], to.name)
      if (curRouterObj && curRouterObj.access !== undefined) {
        // 需要判断权限的路由
        if (curRouterObj.access === parseInt(storage.get('access'))) {
          // 如果在地址栏输入的是一级菜单则默认打开其第一个二级菜单的页面
          Util.toDefaultPage([...routers], to, router, next)
        } else {
          next({
            replace: true,
            name: 'error-403'
          })
        }
      } else {
        // 没有配置权限的路由, 直接通过
        Util.toDefaultPage([...routers], to, router, next)
      }
    }
  }
})

router.afterEach((to) => {
  Util.openNewPage(router.app, to.name, to.params, to.query)
  iView.LoadingBar.finish()
})

export default router
