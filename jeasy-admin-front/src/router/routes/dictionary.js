export default [
  {
    path: '/dictionary',
    title: '公共码表',
    name: 'dictionary',
    icon: 'ios-book',
    component: () => import('@/app/dictionary')
  }
]
