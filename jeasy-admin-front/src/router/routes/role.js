export default [
  {
    path: '/role',
    title: '角色管理',
    name: 'role',
    icon: 'ios-person',
    component: () => import('@/app/role')
  }
]
