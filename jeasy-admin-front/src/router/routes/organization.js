export default [
  {
    path: '/organization',
    title: '组织机构',
    name: 'organization',
    icon: 'ios-people',
    component: () => import('@/app/organization')
  }
]
