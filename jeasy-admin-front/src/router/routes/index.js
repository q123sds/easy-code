import fileAttach from './fileAttach'
import log from './log'
import organization from './organization'
import resource from './resource'
import role from './role'
import user from './user'
import dictionary from './dictionary'
import code from './code'
import monitor from './monitor'
import Main from '@/views/Main'

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在adminRouter里
export const adminRouter = [
  {
    path: '/',
    name: 'userManagement',
    title: '用户管理',
    icon: 'ios-people',
    component: Main,
    children: [
      ...user,
      ...role,
      ...resource,
      ...organization
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'confManagement',
    title: '基础数据',
    icon: 'settings',
    component: Main,
    children: [
      ...dictionary,
      ...fileAttach
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'codeManagement',
    title: '代码平台',
    icon: 'code-working',
    component: Main,
    children: [
      ...code
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'monitorManagement',
    title: '日志监控',
    icon: 'monitor',
    component: Main,
    children: [
      ...log,
      ...monitor
    ],
    meta: {
      requiresAuth: true
    }
  }
]
