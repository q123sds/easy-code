import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { appRouter, otherRouter } from '@/router/iviewAdmin'
import { adminRouter } from '@/router/routes'

export default {
  state: {
    cachePage: [],
    lang: '',
    isFullScreen: false,
    // 要展开的菜单数组
    openedSubmenuArr: [],
    // 主题
    menuTheme: 'dark',
    themeColor: '',
    pageOpenedList: [{
      title: '首页',
      path: '',
      name: 'home_index'
    }],
    currentPageName: '',
    // 面包屑数组
    currentPath: [
      {
        title: '首页',
        path: '',
        name: 'home_index'
      }
    ],
    menuList: [],
    routers: [
      otherRouter,
      ...appRouter,
      ...adminRouter
    ],
    tagsList: [...otherRouter.children],
    messageCount: 0,
    // 在这里定义你不想要缓存的页面的name属性值(参见路由配置router.js)
    dontCache: ['text-editor', 'artical-publish']
  },
  getters,
  actions,
  mutations
}
